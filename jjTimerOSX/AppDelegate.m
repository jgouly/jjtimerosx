//
//  AppDelegate.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 23/08/2013.
//
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.currentScrambler = [Scrambler3x3 new];
    self.timer = [Timer new];
    self.scrambleLabel.stringValue = [self.currentScrambler scramble];
    self.session = [Session new];
    [self.tbl setDataSource:self];
}

- (void)keyDown:(NSEvent *)event {
    if ([event.characters isEqualToString:@" "]) {
        [self onTimerTrigger];
    } else if (event.keyCode == 53) {
        [self.session reset];
        [self.tbl reloadData];
        [self resetStats];
    }
}

- (void)keyUp:(NSEvent *)event {
    if ([event.characters isEqualToString:@" "]) {
        [self onTimerTrigger];
    }
}

- (void) resetStats {
    self.bestTimeLabel.stringValue = @"-";
    self.currentAverage5Label.stringValue = @"-";
}

- (void) updateStats {
    self.bestTimeLabel.stringValue = [self formatTime:[self.session findBestTime:0 endIndex:[self.session count]]];
    self.currentAverage5Label.stringValue = [self formatTime:[self.session findCurrentAverage:5]];
}

- (NSString*)formatTime:(long) time {
    if (time == -1) return @"DNF";
    time = time / 10;
    NSUInteger bits = time % 100;
    time = (time - bits) / 100;
    NSUInteger seconds = time % 60;
    
    return [NSString stringWithFormat:@"%lu.%02lu", seconds, bits];
}

- (void) setTimerLabelValue:(long) time {
    self.timerLabel.stringValue = [self formatTime:time];
}

- (void)timerFired:(NSTimer *)timer {
    [self setTimerLabelValue:[self.timer currentTime]];
}

- (void) startUIUpdater {
    self.interfaceTimer = [NSTimer scheduledTimerWithTimeInterval:0.001
                                                           target:self
                                                         selector:@selector(timerFired:)
                                                         userInfo:nil repeats:YES];
}

- (void) stopUIUpdater {
    [self.interfaceTimer invalidate];
    self.interfaceTimer = nil;
}

- (void) onTimerTrigger {
    if ([self.timer isRunning]) {
        [self.timer stop];
        [self stopUIUpdater];
        [self setTimerLabelValue:[self.timer currentTime]];
        [self.session addTime:[self.timer currentTime]];
        [self.tbl reloadData];
        [self.tbl scrollRowToVisible:[self.tbl numberOfRows]-1];
        self.scrambleLabel.stringValue = [self.currentScrambler scramble];
        [self updateStats];
        [NSTimer scheduledTimerWithTimeInterval:0.1
                                         target:self
                                       selector:@selector(resetTimer:)
                                       userInfo:nil repeats:NO];
    } else if ([self.timer isReady]) {
        [self.timer start];
        [self startUIUpdater];
    } else if ([self.timer isWaiting]) {
        [self.timer ready];
    }
}

- (void) resetTimer:(NSTimer*) timer {
    [self.timer waiting];
}

- (id) tableView:(NSTableView *) aTableView
objectValueForTableColumn:(NSTableColumn *) aTableColumn
             row:(NSInteger) rowIndex
{
    
    return [self formatTime:[self.session timeAtIndex:rowIndex]];
}

- (NSInteger) numberOfRowsInTableView:(NSTableView *)aTableView
{
    return [self.session count];
}

@end
