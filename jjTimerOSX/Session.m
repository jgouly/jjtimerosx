//
//  Session.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 29/08/2013.
//
//

#import "Session.h"

@implementation Session

- (id) init {
    self = [super init];
    self.times = [NSMutableArray new];
    return self;
}

- (long) timeAtIndex:(NSInteger) index {
    return [[self.times objectAtIndex:index] longValue];
}

- (int) count {
    return (int) [self.times count];
}

- (void) addTime:(long)time {
    [self.times addObject:[NSNumber numberWithLong:time]];
}

- (void) reset {
    [self.times removeAllObjects];
}

// FIXME: -1 for DNFs won't work here, use INT_MAX?
- (long) findBestTime:(int)startIndex endIndex:(int)endIndex {
    long minVal = [self timeAtIndex:startIndex];
    for(int i = startIndex; i < endIndex; ++i) {
        if ([self timeAtIndex:i] < minVal)
            minVal = [self timeAtIndex:i];
    }
    return minVal;
}

- (long) calculateAverage:(int) startIndex endIndex:(int) endIndex {
    if (startIndex < 0) return -1;
    if (endIndex == 0) return -1;
    if ((endIndex - startIndex) == 0) return -1;
    long average = 0;
    for(int i = startIndex; i < endIndex; ++i) {
        average += [self timeAtIndex:i];
    }
    return average / (endIndex - startIndex);
}

- (long) findCurrentAverage:(int)size {
    return [self calculateAverage:(int)([self.times count] - (long) size) endIndex:(int)[self.times count]];
}

@end