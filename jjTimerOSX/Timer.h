//
//  Timer.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import <Foundation/Foundation.h>

@interface Timer : NSObject

- (void) waiting;
- (BOOL) isWaiting;
- (void) ready;
- (BOOL) isReady;
- (void) start;
- (BOOL) isRunning;
- (void) stop;
- (long) currentTime;

enum State {
    WAITING,
    READY,
    RUNNING,
    STOPPED
};

@property enum State state;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;

@end