//
//  GenericScrambler.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import "Scrambler.h"

@interface GenericScrambler : Scrambler

- (id) initGeneric:(int) len suffixes:(NSArray *) suffixes turns:(NSArray *) turns;

@property unsigned len;
@property NSArray *suffixes;
@property NSArray *turns;

@end
