//
//  Scrambler3x3.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import <Foundation/Foundation.h>

#import "GenericScrambler.h"

@interface Scrambler3x3 : GenericScrambler

@end
