//
//  Session.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 29/08/2013.
//
//

#import <Foundation/Foundation.h>

@interface Session : NSObject

- (long) timeAtIndex:(NSInteger) index;
- (int) count;
- (void) addTime:(long) time;
- (void) reset;
- (long) findBestTime:(int) startIndex endIndex:(int) endIndex;
- (long) findCurrentAverage:(int) size;

@property NSMutableArray *times;

@end
