//
//  AppDelegate.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 23/08/2013.
//
//

#import <Cocoa/Cocoa.h>

#import "Scrambler.h"
#import "Scrambler3x3.h"
#import "Timer.h"
#import "Session.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *timerLabel;
@property (assign) IBOutlet NSTextField *scrambleLabel;
@property (assign) IBOutlet NSTableView *tbl;

// statistics
@property (assign) IBOutlet NSTextField *bestTimeLabel;
@property (assign) IBOutlet NSTextField *currentAverage5Label;

@property (strong) Scrambler *currentScrambler;
@property (strong) Timer *timer;
@property (strong) Session *session;
@property (nonatomic, strong) NSTimer *interfaceTimer;

- (void) keyDown:(NSEvent *) event;
- (void) keyUp:(NSEvent *) event;
- (NSString *) formatTime:(long) time;

@end
