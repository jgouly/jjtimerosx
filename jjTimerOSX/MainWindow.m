//
//  MainWindow.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import "MainWindow.h"
#import "AppDelegate.h"

@implementation MainWindow

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)keyDown:(NSEvent *)event {
    AppDelegate *appDelegate = [NSApp delegate];
    [appDelegate keyDown:event];
}

- (void)keyUp:(NSEvent *)event {
    AppDelegate *appDelegate = [NSApp delegate];
    [appDelegate keyUp:event];
}

@end
