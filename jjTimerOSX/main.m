//
//  main.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 23/08/2013.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
