//
//  Timer.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import "Timer.h"

@implementation Timer

- (id) init {
    self = [super init];
    self.state = WAITING;
    return self;
}

- (void) waiting {
    self.state = WAITING;
}

- (BOOL) isWaiting {
    return self.state == WAITING;
}

- (void) ready {
    self.state = READY;
}

- (BOOL) isReady {
    return self.state == READY;
}

- (void) start {
    self.startTime = [NSDate date];
    self.state = RUNNING;
    self.endTime = nil;
}

- (BOOL) isRunning {
    return self.state == RUNNING;
}

- (void) stop {
    self.endTime = [NSDate date];
    self.state = STOPPED;
}

- (long) currentTime {
    NSDate *time = self.endTime ? self.endTime : [NSDate date];
    return -[self.startTime timeIntervalSinceDate:time]*1000;
}

@end
