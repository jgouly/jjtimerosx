//
//  Scrambler3x3.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import "Scrambler3x3.h"

@implementation Scrambler3x3

- (id) init {
    self = [super initGeneric:25 suffixes:@[@" ",@"2 ",@"' "] turns:@[@[@"U",@"D"], @[@"F",@"B"], @[@"R",@"L"]]];
    return self;
}

@end