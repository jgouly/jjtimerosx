//
//  GenericScrambler.m
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import "GenericScrambler.h"

@implementation GenericScrambler

- (id) initGeneric:(int) len suffixes:(NSArray *) suffixes turns:(NSArray *) turns {
    self = [super init];
    self.len = len;
    self.suffixes = suffixes;
    self.turns = turns;
    return self;
}

- (NSString *)scramble {
	int lastaxis = -1;
	unsigned long arySize = [[self.turns valueForKeyPath:@"@max.@count"] unsignedLongValue];
	int *donemoves = malloc(arySize*sizeof(int));
	NSMutableString *s = [[NSMutableString alloc] initWithString:@""];
	for (int i=0; i < self.len; i++) {
        int done = 0;
		do {
			int first = arc4random() % [self.turns count];
			int second = arc4random() % [[self.turns objectAtIndex:first] count];
			
			if (first != lastaxis) {
				for (int k=0; k < [[self.turns objectAtIndex:first] count]; k++){
					donemoves[k] = 0;
				}
				lastaxis = first;
			}
			if (donemoves[second] != 1) {
				donemoves[second] = 1;
				if ([[[self.turns objectAtIndex:first] objectAtIndex:second] isKindOfClass:[NSArray class]]) {
					[s appendFormat:@"%@%@",[self randEl:[[self.turns objectAtIndex:first] objectAtIndex:second]], [self randEl:self.suffixes]];
				}
				else {
					[s appendFormat:@"%@%@",[[self.turns objectAtIndex:first] objectAtIndex:second], [self randEl:self.suffixes]];
				}
				done = 1;
			}
		} while (done == 0);
	}
	free(donemoves);
	return [NSString stringWithString:s];
}
- (NSString *)randEl:(NSArray *)arr{
	return [arr objectAtIndex:(arc4random() % [arr count])];
}

@end
