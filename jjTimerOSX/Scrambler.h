//
//  Scrambler.h
//  jjTimerOSX
//
//  Created by Joey Gouly on 26/08/2013.
//
//

#import <Foundation/Foundation.h>

@interface Scrambler : NSObject

- (NSString *) scramble;

@end